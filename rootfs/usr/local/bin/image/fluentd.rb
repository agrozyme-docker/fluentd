# require_relative('../docker_core')
require('docker_core')

module DockerCore
  module Image
    module Fluentd
      module Build
        def self.main
          System.run('apk add --no-cache --virtual .build build-base ruby-dev')
          System.run('gem install --no-document fluentd fluent-plugin-forest fluent-plugin-rewrite-tag-filter fluent-plugin-script fluent-plugin-influxdb fluent-plugin-elasticsearch')
          # System.run('gem install --no-document fluentd fluent-plugin-forest fluent-plugin-rewrite-tag-filter fluent-plugin-script')
          System.run('apk del --no-cache .build')
          Shell.make_folders('/usr/local/etc/fluent')
          System.run('fluentd --dry-run')
        end
      end

      module Run
        def self.main
          Shell.update_user
          Shell.make_folders('/var/log/fluent')
          System.execute('fluentd', sudo: USER)
        end
      end
    end
  end
end
