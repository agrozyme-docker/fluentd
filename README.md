# Summary

Source: https://gitlab.com/agrozyme-docker/fluentd

Fluentd is an open source data collector for unified logging layer

# Settings

- Log Directory: `/var/log/fluent`
- Default Configuration Directory: `/etc/fluent`
- Custom Configuration Files: `/usr/local/etc/fluent/*.conf`

# Example Configuration Files

- Store in source code repository folder: `example`
- `fluent.conf`: run `fluentd --setup` to generate the default configuration file
- Other configuration files:
  - `forward.conf`: input forward, output file
  - `tail.file.conf`: input tail, output file
  - `tail.elasticsearch.conf`: input tail, output elasticsearch

# Usage:
- `forward.conf`:

  - `docker-compose.yml` setting:

  ```yml
  logging:
    driver: 'fluentd'
    options:
      fluentd-address: 'localhost:24224'
      fluentd-async-connect: 'true'
      tag: docker.{{.Name}}
  ```

- Other configuration files:

  - for the `fluentd` container `docker-compose.yml` setting:

  ```yml
  volumes:
    - /var/lib/docker/containers:/var/lib/docker/containers/
  ```

  - for other containers `docker-compose.yml` setting:

  ```yml
  logging:
    options:
      tag: docker.{{.Name}}
  ```
